﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {
    static string playerTag = "Player";

    Animator animator;

    // Use this for initialization
    void Start() {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        if(animator.GetInteger("DoorState") == 1 && Input.GetKeyDown(KeyCode.F)) {
            animator.SetInteger("DoorState", 2);
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log("enter");
        if(collision.gameObject.tag.Equals(playerTag)) {
            animator.SetInteger("DoorState", 1);
        }
    }

    void OnTriggerStay2D(Collider2D collision) {

    }

    void OnTriggerExit2D(Collider2D collision) {
        if(collision.gameObject.tag.Equals(playerTag)) {
            animator.SetInteger("DoorState", 0);
        }
    }
}
