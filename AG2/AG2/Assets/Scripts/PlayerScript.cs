﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {
    //public Camera mainCamera;
    public float defaultZ = -0.9f;
    public float speed;

    Animator animator;

    float moveX, moveY;

    // Use this for initialization
    void Start() {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        moveX = Input.GetAxisRaw("Horizontal");
        moveY = Input.GetAxisRaw("Vertical");
        CheckState();
    }

    void FixedUpdate() {
        if(!moveX.Equals(0) || !moveY.Equals(0)) {
            Vector3 move = new Vector3(speed * moveX, speed * moveY / 2, 0);
            gameObject.transform.position += move;
        }
    }

    void CheckState() {
        if(Input.GetKeyDown(KeyCode.E)) {
            animator.SetInteger("GirlState", 3);
        }

        if(!moveX.Equals(0) || !moveY.Equals(0)) {
            if(moveY > 0) {
                animator.SetInteger("GirlState", 2);
                transform.localScale = new Vector3(Mathf.Sign(moveX), 1, 1);
            } else {
                animator.SetInteger("GirlState", 1);
                transform.localScale = new Vector3(-Mathf.Sign(moveX), 1, 1);
            }
        }

        if(moveX.Equals(0) && moveY.Equals(0) && animator.GetInteger("GirlState") != 3) {
            animator.SetInteger("GirlState", 0);
        }
    }

    public void setDefaultLayer() {
        setLayer(defaultZ);
    }

    public void setLayer(float z) {
        transform.position = new Vector3(transform.position.x, transform.position.y, z);
    }
}

