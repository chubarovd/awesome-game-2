﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulidingScript : MonoBehaviour {
    public GameObject topLayer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D collision) {
        collision.gameObject.GetComponent<PlayerScript>().setLayer(-0.2f);
        topLayer.GetComponent<Animator>().SetBool("isInvisible", true);
    }

    void OnTriggerExit2D(Collider2D other) {
        other.gameObject.GetComponent<PlayerScript>().setDefaultLayer();
        topLayer.GetComponent<Animator>().SetBool("isInvisible", false);
    }
}
